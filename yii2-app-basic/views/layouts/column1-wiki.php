<?php 
use \yii\widgets\Block;
use kartik\sidenav\SideNav;


$this->blocks['sidebar'] = '';
$this->blocks['toolbar'] = '';

//  //layouts/main
$this->beginContent('@app/views/layouts/column2.php'); ?>
<!--<div class="container">
	<div class="span-6">-->
		<p>
			
<?php Block::begin(array('id'=>'sidebar')); ?>
<?php

 // <editor-fold defaultstate="collapsed" desc="sidebar nav">
echo SideNav::widget([
    'type' => SideNav::TYPE_DEFAULT,
    'heading' => Yii::t('app','Wiki Items'),
    'items' => 
    [
        [
            'url' => ['/wiki/index'],
            'label' => 'Wiki Home',
            'icon' => 'home'
        ],   
        [
            'url' => ['/wiki/faq'],
            'label' => 'FAQ',
            'icon' => ''
        ],                    
        [
            'url' => ['/wiki/extensions'],
            'label' => 'Extensions',
            'icon' => ''
        ],                    
        [
            'url' => ['/wiki/database-relations'],
            'label' => 'Database Relations',
            'icon' => ''
        ],
        [
            'url' => ['/wiki/database-requirements'],
            'label' => 'Database Requirements',
            'icon' => ''
        ],                         
        [
                'label' => 'Extension Usage Tips',
                'icon' => '',
                'items' => [
                        ['label' => 'Migration Creator', 'icon'=>''
                            , 'url'=>['/wiki/migration-creator']],
                        ['label' => 'Save Relations Behavior', 'icon'=>'',
                            'url'=>['/wiki/save-relations']],
                ],
        ],                    
                    
                   // ['label' => Yii::t('app','Create'), 'icon'=>'plus', 'url'=>['create']]
                  ]
        ]);
// </editor-fold>
?>
   
<?php Block::end(); ?>
		</p>

<?php Block::begin(array('id'=>'toolbar')); ?>  
<?php 
/*
echo SideNav::widget([
    'type' => SideNav::TYPE_DEFAULT,
    'heading' => Yii::t('app','Toolbar'),
    'items' => 
    [
        [
            'url' => ['/wiki/index'],
            'label' => 'Wiki Home',
            'icon' => 'home'
        ],   
        [
            'url' => ['/wiki/faq'],
            'label' => 'FAQ',
            'icon' => ''
        ],                    
        [
            'url' => ['/wiki/extensions'],
            'label' => 'Extensions',
            'icon' => ''
        ],                    
        [
            'url' => ['/wiki/database-relations'],
            'label' => 'Database Relations',
            'icon' => ''
        ],
        [
            'url' => ['/wiki/database-requirements'],
            'label' => 'Database Requirements',
            'icon' => ''
        ],                         
        [
                'label' => 'Installed Yii2 Extensions',
                'icon' => '',
                'items' => [
                        ['label' => 'yii2 Migration Creator', 'icon'=>''
                            , 'url'=>['/wiki/migration-creator']],
                        ['label' => 'yii2 Save Relations Behavior', 'icon'=>'',
                            'url'=>['/wiki/save-relations']],
                ],
        ],                    
                    
                    ['label' => Yii::t('app','Create'), 'icon'=>'plus', 'url'=>['create']]
                  ]
        ]);
 * 
 */
?>                
                
<?php Block::end(); ?>
                
<?php echo $content; ?>

<?php $this->endContent(); ?>

