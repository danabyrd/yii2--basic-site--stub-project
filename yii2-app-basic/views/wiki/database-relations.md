Camps are related to
Players, Coaches, Teams, Camp Sports, Camp Sessions, and Player Camp Attendance Logs

Org
* Camp
* Media
* Note
* Payment Log
* Person
* School
* Subscription
* Team


The Controller level looks like this
Camp
* CampCoach
* CampLocation
* CampSession
* CampSport
* PlayerCampLog
* Team
