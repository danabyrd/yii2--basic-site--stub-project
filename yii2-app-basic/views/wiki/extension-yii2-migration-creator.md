see:  https://github.com/bizley/yii2-migration

# Usage
The following console command are available:

List all the tables in the database:

```php yii migration```
or

```shell
php yii migration/list
```
Generate migration to create DB table table_name:

```sh
php yii migration/create table_name
```
Generate migrations to create all DB tables:

```sh
php yii migration/create-all
```
Generate migration to update DB table table_name:

php yii migration/update table_name
Generate migrations to update all DB tables:

```
php yii migration/update-all
```

You can generate multiple migrations for many tables at once by separating the names with a comma:

```
php yii migration/create table_name1,table_name2,table_name3
```

## Updating migration
Starting with yii2-migration v2.0 it is possible to generate updating migration for database table.

History of applied migrations is scanned to gather all modifications made to the table.
Virtual table schema is prepared and compared with current table schema.
Differences are generated as update migration.
In case of migration history not keeping information about the table creating migration is generated.
Command line parameters
command	alias	description
db		Application component's ID of the DB connection to use when generating migrations. default: 'db'
migrationPath	p	Directory storing the migration classes. default: '@app/migrations'
migrationNamespace	n	Namespace in case of generating namespaced migration. default: null
templateFile	F	Template file for generating create migrations. default: '@bizley/migration/views/create_migration.php'
templateFileUpdate	U	Template file for generating update migrations. default: '@bizley/migration/views/update_migration.php'
useTablePrefix	P	Whether the table names generated should consider the tablePrefix setting of the DB connection. default: 1
migrationTable	t	Name of the table for keeping applied migration information. default: '{{%migration}}'
showOnly	s	Whether to only display changes instead of generating update migration. default: 0
generalSchema	g	Whether to use general column schema instead of database specific (1). default: 1
fixHistory	h	Whether to add migration history entry when migration is generated. default: 0
skipMigrations		List of migrations from the history table that should be skipped during the update process (2). default: []
(1) Remember that with different database types general column schemas may be generated with different length.

### MySQL examples:
Column varchar(45)
generalSchema=0: $this->string(45)
generalSchema=1: $this->string()

Column int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY
generalSchema=0: $this->integer(11)->notNull()->append('AUTO_INCREMENT PRIMARY KEY')
generalSchema=1: $this->primaryKey()

(2) Here you can place migrations containing actions that can not be covered by extractor i.e. when there is a migration setting the RBAC hierarchy with authManager component. Such actions should be kept in separated migration and placed on this list to prevent them from being run during the extraction process.

# Renaming
When you rename table or column remember to generate appropriate migration manually otherwise this extension will not generate updating migration (in case of the table) or will generate migration with command to drop original column and add renamed one (in case of the column). This is happening because yii2-migration can only compare two states of the table without the knowledge of how one state turned into another. And while the very result of migration renaming the column and the one dropping it and adding another is the same in terms of structure, the latter makes you lose data.

Once you add renaming migration to the history it's being tracked by the extension.

