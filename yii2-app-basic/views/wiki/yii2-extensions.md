# Yii2 Extensions Documentation

## Yii2 Extensions by Category

**auth**

- [yii2-rbac](https://github.com/dektrium/yii2-rbac)
- yii2-user ([github](https://github.com/dektrium/yii2-user))

**database**
- [yii2-encrypter](https://www.yiiframework.com/extension/yii2-encrypter)
- yii2-migration ([github](https://github.com/bizley/yii2-migration)) ([stp wiki](extension-yii2-migration-creator))
- [yii2-save-relations-behavior](https://github.com/la-haute-societe/yii2-save-relations-behavior) ([stp wiki](extension-yii2-save-relations-behavior))

**network**
- [yii2-curl](https://github.com/linslin/Yii2-Curl/)
- [yii2-nested-rest](https://github.com/tunecino/yii2-nested-rest)

**user interface**
- [yii2-barcode-generator](https://github.com/Vilochane/Yii-Barcode-Generator)
- [yii2-enhanced-gii](https://github.com/mootensai/yii2-enhanced-gii)
- [yii2-gravatar](https://github.com/cebe/yii2-gravatar)
- [yii2-rrssb](https://github.com/imanilchaudhari/yii2-rrssb) Responsive social share bar widget

## STP Yii2 Extensions by Name

- [yii2-audit](https://github.com/bedezign/yii2-audit/)
- [yii2-barcode-generator](https://github.com/Vilochane/Yii-Barcode-Generator)
- [yii2-curl](https://github.com/linslin/Yii2-Curl/)
- [yii2-encrypter](https://www.yiiframework.com/extension/yii2-encrypter)
- [yii2-enhanced-gii](https://github.com/mootensai/yii2-enhanced-gii)
- [yii2-gravatar](https://github.com/cebe/yii2-gravatar)
- [yii2-migration-creator](https://github.com/bizley/yii2-migration)
- [yii2-nested-rest](https://github.com/tunecino/yii2-nested-rest)
- [yii2-rbac](https://github.com/dektrium/yii2-rbac)
- [yii2-rrssb](https://github.com/imanilchaudhari/yii2-rrssb) Responsive social share bar widget
- [yii2-save-relations-behavior](https://github.com/la-haute-societe/yii2-save-relations-behavior) ([stp wiki](extension-yii2-save-relations-behavior))
- [yii2-user](https://github.com/dektrium/yii2-user)

## STP Common Architecture - Yii2 Extensions by Namespace and Name
- [php": ">=5.4.0"](http://www.php.net/)
- [bedezign/yii2-audit:"^1.0"](https://github.com/bedezign/yii2-audit/)
- [yiisoft/yii2": "~2.0.14"](https://www.yiiframework.com/)
- [yiisoft/yii2-bootstrap: "~2.0.0"](https://github.com/yiisoft/yii2-bootstrap)
- [yiisoft/yii2-swiftmailer: "~2.0.0"](https://yiigist.com/package/yiisoft/yii2-swiftmailer#!?tab=readme)
- [bizley/yii2-migration: "~3.0.0"](https://github.com/bizley/yii2-migration)
- [dektrium/yii2-user: "^0.9.14"](https://github.com/dektrium/yii2-user)
- [dektrium/yii2-rbac: "1.0.0-alpha@dev"](https://github.com/dektrium/yii2-rbac)
- [imanilchaudhari/yii2-rrssb "dev-master"](https://github.com/imanilchaudhari/yii2-rrssb)
- [tunecino/yii2-nested-rest: "^0.3.0"](https://github.com/tunecino/yii2-nested-rest)
- [mootensai/yii2-enhanced-gii: "dev-master"](https://github.com/mootensai/yii2-enhanced-gii)
- [kartik-v/yii2-tree-manager: "dev-master"](https://github.com/kartik-v/yii2-tree-manager)
- [linslin/yii2-curl: "*"](https://github.com/linslin/Yii2-Curl/)
- [nickcv/yii2-encrypter: "*"](https://www.yiiframework.com/extension/yii2-encrypter)
- [cebe/yii2-gravatar: "^1.1"](https://github.com/cebe/yii2-gravatar)
- [la-haute-societe/yii2-save-relations-behavior" "*"](https://github.com/la-haute-societe/yii2-save-relations-behavior) ([stp wiki](extension-yii2-save-relations-behavior))
- [vilochane/yii2-barcode-generator": "dev-master"](https://github.com/Vilochane/Yii-Barcode-Generator)

