# STP Installation
To Use Any STP project just follow these simple steps to get your programmer started. 

**Now you can have working stub application with four lines of command line code**

1. git clone {project URL} {optional-folder-name}
2. cd {project folder} 
3. composer update
4. yii migration

**Example 1 - use default project name**

```
git clone https://gitlab.com/sport-testing-portal/stp--basic-site--schema-v2.git 
cd stp--basic-site--schema-v2
composer update
yii migration
config apache2 or run "./yii serve"
```

**Example 2 - create a custom project name**

```
git clone https://gitlab.com/sport-testing-portal/stp--basic-site--schema-v2.git myprojectOne
cd myprojectOne
composer update
yii migration
config apache2 or run "./yii serve"
```


# Linux Permissions
If you're developing on a Linux workstation you will need to adjust the folder permissions for the project. Your own username will by filled in by the Linux **$USER** variable when you run these commands.

Change the 'group' of the code generation folders. 

```shell
sudo chown -Rcv $USER:www-data models, controllers, views

# or use these lines for more concise and more readable output
sudo chown -Rcv $USER:www-data models
sudo chown -Rcv $USER:www-data controllers
sudo chown -Rcv $USER:www-data views
```

Change the 'group' of the runtime folder where log files are saved.

```shell
sudo chown -Rcv www-data:www-data runtime
```



**If you're running Apache2 as your web server**

- Are you a member of the Apache2 group eg www-data?
- Is your username in the output of this command?

```shell
grep "^www-data" /etc/group
```

If you do see your username then you are already in the www-data group. **If you do not see your username** you can easily add yourself using the sudo command.

How to add yourself to the www-data group on your development workstation

```shell
sudo useradd -G www-data
```

## Handy Shell Aliases on Linux

```sh
alias ch400='sudo chmod 400 $1'
alias ch400i='sudo chmod 400 ~/.ssh/id_rsa.pub'
alias ch400r='sudo chmod 400 /root/.ssh/id_rsa.pub;o /root/.ssh;'
alias chmod_files='find -maxdepth 20 -type f -exec chmod 644 {} \;'
alias chmod_folders='find -maxdepth 20 -type d -exec chmod 755 {} \;'
alias cmd1776='find -maxdepth 20 -type d -exec sudo chmod --changes 1776 {} \;'
alias cmd2765='find -maxdepth 20 -type d -exec sudo chmod --changes 2765 {} \;'
alias cmd2775='find -maxdepth 20 -type d -exec sudo chmod --changes 2775 {} \;'
alias cmd2776='find -maxdepth 20 -type d -exec sudo chmod --changes 2776 {} \;'
alias cmd6775='find -maxdepth 20 -type d -exec sudo chmod --changes 6775 {} \;'
alias cmd765='find -maxdepth 20 -type d -exec sudo chmod --changes 765 {} \;'
alias cmd775='find -maxdepth 20 -type d -exec sudo chmod --changes 755 {} \;'
alias cmf2775='find -maxdepth 20 -type f -exec sudo chmod --changes 2775 {} \;'
alias cmf2776='find -maxdepth 20 -type f -exec sudo chmod --changes 2776 {} \;'
alias cmf644='find -maxdepth 20 -type f -exec sudo chmod --changes 644 {} \;'
alias cmf765='find -maxdepth 20 -type f -exec sudo chmod --changes 765 {} \;'
alias cmf775='find -maxdepth 20 -type f -exec sudo chmod --changes 775 {} \;'
alias cmf776='find -maxdepth 20 -type f -exec sudo chmod --changes 776 {} \;'
```

Use alias ```cmd2776='find -maxdepth 20 -type d -exec sudo chmod --changes 2776 {} \;'``` on models, controllers, and views folders for effortless code generation.

## Chown

```sh
alias cod='find -maxdepth 20 -type d -exec sudo chown --changes dbyrd:www-data {} \;'
alias codww='find -maxdepth 20 -type d -exec sudo chown --changes www-data:www-data {} \;'
alias cof='find -maxdepth 20 -type f -exec sudo chown --changes dbyrd:www-data {} \;'
alias cofww='find -maxdepth 20 -type f -exec sudo chown --changes www-data:www-data {} \;'
alias gown='sudo chown -Rc --changes $UID .git/'
alias gownd='find . -not -uid $UID -ls -exec chown --changes $UID {} +'
```

Use alias **cod**, and **cof** for models, controllers, and  views for effortless code generation.

Setup project folders for Gii code generation on Linux

```sh
#!/bin/bash

cd models
find -maxdepth 20 -type d -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type f -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type d -exec sudo chmod --changes 2776 {} \;
find -maxdepth 20 -type f -exec sudo chmod --changes 2776 {} \;

cd ../views
find -maxdepth 20 -type d -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type f -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type d -exec sudo chmod --changes 2776 {} \;
find -maxdepth 20 -type f -exec sudo chmod --changes 2776 {} \;

cd ../controllers
find -maxdepth 20 -type d -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type f -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type d -exec sudo chmod --changes 2776 {} \;
find -maxdepth 20 -type f -exec sudo chmod --changes 2776 {} \;

echo "gii is ready to generate code"
```


# Handy Bash Aliases for Working with Apache2

```sh
alias aa='c; ls /etc/apache2/sites-enabled; ll /etc/apache2/sites-available'
alias ama='cd /etc/apache2/mods-available/'
alias ap='sudo nano /etc/apache2/ports.conf'
alias apache_error='sudo tail -f  /var/log/apache2/error.log'
alias ar='sudo service apache2 reload'
alias ars='sudo /etc/init.d/apache2 restart'
alias as='sudo service apache2 stop'
alias asa='cd /etc/apache2/sites-available/'
alias cda='cd /etc/apache2/'
```


# Get Started With Coding
The first step is to logon on as admin/admin. You'll notice a 'code generate' menu option that I added for the admin. Code gen only works on 127.0.0.1 or an IP address you specify.


**Login as admin/admin then go to 'code generate' on the menu**.
![login_as_admin_then_go_to__code_generate__on_the_menu](/uploads/0aeeb16b2f84a93f4005d11ab5f1c907/login_as_admin_then_go_to__code_generate__on_the_menu.png)


The Yii2 code generator will appear. **Next we gen models, and views** that are pre-wired with Bootstrap and pre-wired to the models. 

![A_customized_Yii2_code_generator](/uploads/7377369e230bd7572178e0a10fda57c7/A_customized_Yii2_code_generator.png)

**Then we add menu items** for your generated forms. Next we **add permissions** about which user groups can 'see' the menu items and **'run' the forms**. 


# STP Common Architecture 
[Installed Yii2 Extensions](yii2-extensions)

[MySQL Foreign Relation Documentation](https://dev.mysql.com/doc/refman/8.0/en/create-table-foreign-keys.html)

[Database Requirements](database-requirements)

# Update models moved from a basic project to an advanced project

```cd common/models```

```grep -rl 'namespace app' *php . | xargs sed -i 's/namespace app/namespace common/g'```

@todo Add installation tips and operations tips