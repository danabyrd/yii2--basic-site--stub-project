<?php

return [
    'adminEmail' => 'admin@example.com',
    'application-name' => 'My Application',
    'company-name'     => 'My Company',
    'version'          => '0.6.0'
];
