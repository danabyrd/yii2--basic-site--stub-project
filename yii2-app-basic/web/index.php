<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';



(new yii\web\Application($config))->run();

    
    /* Test in forms the various aspects of rendered sub-forms. This is only used in views.
	 * The code snippets below are typically placed on the last line of a view file.
	 * Sample output "views/site/index.php"  	
	 * Examples: 
	 *   echo '<pre><code>' . yii\helpers\VarDumper::dumpAsString($model) . '</pre></code>';
	 *   Using the short echo syntax in a view
	 *   <?= echo '<pre><code>' . yii\helpers\VarDumper::dumpAsString($model) . '</pre></code>'; ?>  
	 * @todo: remove from production code projects before deployment
	 */
    function fileWithStubPath($file) {
        $bt   = debug_backtrace();
        //$file_name = basename($bt[0]['file']);
        $file_name = $bt[0]['file'];
        $return = pathinfo(dirname($file_name,2), PATHINFO_BASENAME) .'/' 
            . pathinfo(dirname($file_name,1), PATHINFO_BASENAME) 
            .'/<b>' . basename($file_name) . "</b>\n";
        $return = '<p>' .pathinfo(dirname($file_name,2), PATHINFO_BASENAME) .'/' 
            . pathinfo(dirname($file_name,1), PATHINFO_BASENAME) 
            .'/<b>' . basename($file_name) . "</b></p>";        
        return $return;
    }