#!/bin/bash

cd models
find -maxdepth 20 -type d -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type f -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type d -exec sudo chmod --changes 2776 {} \;
find -maxdepth 20 -type f -exec sudo chmod --changes 2776 {} \;

cd ../views
find -maxdepth 20 -type d -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type f -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type d -exec sudo chmod --changes 2776 {} \;
find -maxdepth 20 -type f -exec sudo chmod --changes 2776 {} \;

cd ../controllers
find -maxdepth 20 -type d -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type f -exec sudo chown --changes $USER:www-data {} \;
find -maxdepth 20 -type d -exec sudo chmod --changes 2776 {} \;
find -maxdepth 20 -type f -exec sudo chmod --changes 2776 {} \;

echo "gii is ready to generate code"
