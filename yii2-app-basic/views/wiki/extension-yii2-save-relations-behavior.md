# What does it do?
Validate and save automatically related Active Record models.

# How does it work?
The following code is added to a model that references Yii2 ActiveRecord

```php
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;

class Project extends \yii\db\ActiveRecord
{
    use SaveRelationsTrait; // Optional

    public function behaviors()
    {
        return [
            'timestamp'     => TimestampBehavior::className(),
            'blameable'     => BlameableBehavior::className(),
            ...
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => [
                    'company',
                    'users',
                    'projectLinks' => ['cascadeDelete' => true],
                    'tags'  => [
                        'extraColumns' => function ($model) {
                            /** @var $model Tag */
                            return [
                                'order' => $model->order
                            ];
                        }
                    ]
                ],
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
```



# Usage
Every declared relations in the relations behavior parameter can now be set and saved as follow:

```php
$project = new Project();
$project->name = "New project";
$project->company = Company::findOne(2);
$project->users = User::findAll([1,3]);
$project->save();
```

You can set related model by only specifying its primary key:

```php
$project = new Project();
$project->name = "Another project";
$project->company = 2;
$project->users = [1,3];
$project->save();
```

You can even set related models as associative arrays like this:

```php
$project = Project::findOne(1);
$project->company = ['name' => 'GiHub', 'description' => 'Awesome']; // Will create a new company record
// $project->company = ['id' => 3, 'name' => 'GiHub', 'description' => 'Awesome']; // Will update an existing company record
$project->save();
```

Attributes of the related model will be massively assigned using the `load() method. So remember to declare the according attributes as safe in the rules of the related model.

Notes:

Related models are saved during the EVENT_BEFORE_VALIDATE event of the owner model. Transaction will start at this point according to the transactions() method of the model.
**Only newly created or changed related models will be saved**.
See the PHPUnit tests for more examples.



## Populate additional junction table columns in a many-to-many relation
In a many-to-many relation involving a junction table additional column values can be saved to the junction table for each model. See the configuration section for examples.

Note: If junction table properties are configured for a relation the rows associated with the related models in the junction table will be deleted and inserted again on each saving to ensure that changes to the junction table properties are saved too.




## Validation
Every declared related models will be validated prior to be saved. If any validation fails, for each related model attribute in error, an error associated with the named relation will be added to the owner model.

For hasMany() relations, the index of the related model will be used to identify the associated error message.

It is possible to specify the validation scenario for each relation by declaring an associative array in which the scenario key must contain the needed scenario value. For instance, in the following configuration, the links related records will be validated using the Link::SOME_SCENARIO scenario:

```php
...
    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => ['company', 'users', 'links' => ['scenario' => Link::SOME_SCENARIO]]
            ],
        ];
    }  
...
```

Tips: **For relations not involving a junction table** by using the via() or viaTable() methods, **you should remove the attributes pointing to the owner model from the 'required' validation rules** to be able to pass the validations.

### Note: 
- **If an error occurs for any reason** during the saving process of related records in the afterSave event, a yii\db\Exception will be thrown on the first occurring error. 
- An **error message will be attached to the relation attribute of the owner model**. 
- In order to be able to handle these cases in a user-friendly way, one will have to catch yii\db\Exception exceptions.




## Delete related records when the main model is deleted
For DBMs with no built in relational constraints, as of 1.5.0 release, one can now specify a relation to be deleted along with the main model.

To do so, the relation should be declared with a property cascadeDelete set to true. For example, related projectLinks records will automaticaly be deleted when the main model will be deleted:

```php
...
'saveRelations' => [
    'class'     => SaveRelationsBehavior::className(),
    'relations' => [
        'projectLinks' => ['cascadeDelete' => true]
    ],
],
...
```
Note: Every records related to the main model as they are defined in their ActiveQuery statement will be deleted.



## Populate the model and its relations with input data
This behavior adds a convenient method to load relations models attributes in the same way that the load() method does. Simply call the loadRelations() with the according input data.

For instance:
```php
$project = Project::findOne(1);
/**
 * $_POST could be something like:
 * [
 *     'Company'     => [
 *         'name' => 'YiiSoft'
 *     ],
 *     'ProjectLink' => [
 *         [
 *             'language' => 'en',
 *             'name'     => 'yii',
 *             'link'     => 'http://www.yiiframework.com'
 *         ],
 *         [
 *             'language' => 'fr',
 *             'name'     => 'yii',
 *             'link'     => 'http://www.yiiframework.fr'
 *         ]
 *     ]
 * ];
 */
$project->loadRelations(Yii::$app->request->post());
```

You can even further simplify the process by adding the SaveRelationsTrait to your model. In that case, a call to the load() method will also automatically trigger a call to the loadRelations() method by using the same data, so you basically won't have to change your controllers.



# Composer Namespace
"la-haute-societe/yii2-save-relations-behavior": "*"

[github](https://github.com/la-haute-societe/yii2-save-relations-behavior)